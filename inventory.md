# Inventory

## Executive Summary

### Boats
- Missing information about the donated single and the status of the old single.
- Urgent need to fill in and optimize pictures (crop and rotate).
- Update on boat "UNNAMED" required. Needs details.
- Missing picture for "Pathfinder" boat (BAL100).

### Blades
- Add new blades from Abingdon.
- Improve pictures.

### Lifejackets
- Incorporate lifejackets in possession of other members (Toby).
- Add lifejackets from Abingdon.
- Verify the functional status of the Seago 150N Classic lifejacket.

### Coxboxes
- Include coxboxes from other locations.
- Update functional status.
- Add any missing coxboxes.
- Update the location of each coxbox.

### Lights
- Overhaul of the entire section.
- Removal of lights not in use.
- Addition of new lights from Abingdon.
- Improved photographs.

### Walkie-Talkies
- Review other walkie-talkies not included in the list.
- Update the status of the set and check functionality.

### Headphones and Microphones
- Requires better photographs.

### Chargers, Cables, and Accessories
- Thoroughly update the section. Remove outdated items and add missing accessories.

### Loanable Kit
- Investigate the status and location of items.

### Ergs
- Overhaul ergs section, including last serviced dates and battery status updates.

### Other
- Locate and account for missing items.
- Update the condition of items where necessary.
- Improve photographs for clarity.

**Action Points for Committee Members:**
1. Update the boat inventory with missing information and pictures.
2. Investigate the status of the donated single and the old single.
3. Address the UNNAMED boat details.
4. Add new blades from Abingdon to the inventory.
5. Overhaul the lights section, including removing unused lights.
6. Gather information from other members about lifejackets and walkie-talkies.
7. Thoroughly update the ergs section, including service history and battery status.
8. Review and update the miscellaneous section, ensuring all items are accounted for.
9. Ensure photographs are improved for clarity in all sections.
10. Create a centralized and up-to-date inventory for the Boat Club to facilitate smooth operations and maintenance.

## Boats

- Notes; *We are missing some information, especially regarding the donated single, also not sure what is happening with the old single. And some pictures need to be filled in ASAP, also should be cropped, and rotated...*

| EA registration | Registered? | Name                 | Type     | Make          | Model   | Belongs to  | Serial No. | Boat Weight | Crew Weight | Manufacture Year | Purchase Price | Comment                                          | Picture                                    |
|-----------------|------------|----------------------|----------|---------------|---------|-------------|------------|-------------|-------------|------------------|----------------|--------------------------------------------------|--------------------------------------------|
| BAL001          | x          | Trevor Gallaher      | 8+      | Empacher      | K86     | Men         | K86P47     | 96kg        | 80-90kg     | 2003             | £27,000        | Three stay riggers                               | ![Image 1](pictures/IMG_0144.jpg)                |
| BAL006          | x          | Baruch S. Blumberg    | 8+      | Aylings       | Carbon Fibre | Women  |             | 1996        | £20,000     |                            |      |                | ![Image 5](pictures/IMG_0146.jpg)                |
| BAL008          | x          |                      | 4+ stern loader | Janousek |         |             |     |      |  65kg        |                         |         |                | ![Image 7](pictures/IMG_0108.jpg)                |
| BAL010          | x          | Happy Rogers II       | 8+      | Empacher      | K84     | Women       | K84Y640    | 96kg        | 70-80kg     | 2011             | £30,000        | Three stay riggers?                              | ![Image 9](pictures/IMG_0115.jpg)                |
| BAL019          |            | Beeland Rogers        | 8+      | Empacher      | R86     | Men         | R86W854    | 96kg        | 80-90kg     | 2009             | £30,000        | Aluminum wing riggers. | ![Image 11](pictures/IMG_0114.jpg)              |
| BAL100          |            | Pathfinder             | 1x      | Swift         | Club B  | Shared      | Unknown    | Unknown     | 70-83kg     | 2017             | £2,580         | Wing rigged, came with bag for rigger?          | ![Image 12](missing)              |
| BAL004          |            | Missing            | 1x      | Missing         | Missing  | Shared      | Unknown    | Unknown     | 70-83kg     |              |        | Currently Sinks I think         | ![Image 12](pictures/IMG_0269.jpg)              |
| BAL103          |            | Dervorguilla             | 1x      | Wintech         | Club B  | Shared      | Unknown    | Unknown     | 70-83kg     | 2013?             | Donated         | Wing rigged         | ![Image 12](missing)              |
| BAL101          | x          | UNNAMED               | 1x      | Ahoy Boats    | TS515   | Shared      | NEEDS UPDATE | NEEDS UPDATE | NEEDS UPDATE | NEEDS UPDATE    | Trainer single                           |        | ![Image 13](pictures/IMG_0114.jpg)              |
| BAL102          | x          | UNNAMED               | 1x      | Ahoy Boats    | TS515   | Shared      | NEEDS UPDATE | NEEDS UPDATE | NEEDS UPDATE | NEEDS UPDATE    | Trainer  single                          |         | ![Image 14](pictures/IMG_0120.jpg)              |
| BAL201          |            | Navigator       | 2-/x    | Swift         | Club B  | Shared      | NEEDS UPDATE | NEEDS UPDATE | 70-85kg     | 2020             | £4,980         | Wing rigged, both sets of riggers              | ![Image 15](missing)              |
| BAL401          |            | Douglas Dupree        | 4+ bowloader | Fillipi | F34     | Shared      | Unknown    | Unknown | 75kg   | 2014             | £18,600        |                                                  | ![Image 16](pictures/IMG_0147.jpg)              |
| BAL402          |            | Neville Mullany       | 4+ bowloader | Fillipi | F34     | Shared      | Unknown    | Unknown |75kg        | 2014             | £18,600        |                                                  | ![Image 17](pictures/IMG_0148.jpg)              |
| BAL801          |            | Missing             | 8+      | Tub       |      | Novices       |   | 96kg        | 80kg        |         |      | Novice Training Tub                         | ![Image 18](pictures/IMG_0145.jpg)              |
| BAL888          |            | Endeavor              | 8+      | Fillipi       | F42     | Men         | F422EB6BG | 96kg        | 80kg        | 2016             | £25,640        | Aluminum wing riggers                           | ![Image 18](missing)              |
| BAL882          | x          | UNNAMED               | 8+      | Hudson        |         | Women       |             |             |             |          |                  |                      | ![Image 19](missing)              |
| BAL030          |            | Peggotty G2           | Launch  | Rowing solutions |       | Shared |        |             |             |              |      |                              | ![Image 20](missing)              |
|                |            | Carl Woodall          | Launch  |               |         | Shared |             |             |       | |      | On its last legs                        |      ![Image 21](missing)              |

## Blades

- Notes; *Got everything bar the new blades in Abingdon and remember some are being sold..., and some more better pictures needed.*

| Type    | Make                  | Model                             | Number in Set | Grip                            | Belongs to      | Measurements      | Cost             | Date of Manufacture | Comment                                     | Picture                                    |
|---------|-----------------------|-----------------------------------|---------------|----------------------------------|-----------------|-------------------|-------------------|---------------------|---------------------------------------------|-------------------------------------------|
| Sculling| Concept 2             |                                   | 2             | Blue Ribbed                      | Shared          |                   |                   |                   | Old Cleaver                                 | ![Image 1](pictures/IMG_1.jpg)               |
| Sculling| Swift                 | Club Performance - Smooth A2     | 2             | 35mm Ribbed Orange               | Shared          | 286-292cm         | £395              | 2017              | Medium Stiffness, 0 Degree Pitch            | ![Image 4](pictures/IMG_4.jpg)               |
| Sculling| Concept 2             | Ultralight - Fat 2               | 4             | Contoured Orange Grip 33.5mm     | Shared          | 278-283cm, Inboard 79-91cm | £940 (for all 4 blades) | 2017 | Medium Stiffness, 0 Degree Pitch | ![Image 5](pictures/IMG_5.jpg) |
| Sweep   | Concept 2             | Ultralight - Smoothie 2         | 8             | Black Suede                      | Men - M2        |                   |                   |                   |                   Old M1 blades. On M2 Rack               | ![Image 6](pictures/IMG_0127.jpg) |
| Sweep   | Concept 2             | Ultralight - Smoothie 2         | 8             | Black Suede                      | Men       |                   |                   |                   | To be sold. On M1 Rack. No collars attached.          | ![Image 6](pictures/IMG_0128.jpg) |
| Sweep   | Concept 2             | Ultralight - Smoothie 2         | 8             | Black with Blue tape            | Men    M3        |                   |                   |                   |                      On M3 Rack                        | ![Image 7](pictures/IMG_0124.jpg) |
| Sweep   | Concept 2             | Ultralight - Smoothie 2         | 8             | Green (Some with Pink Tape)      | Men             |                   |                   |                   | Mixture of Collar Colors                     | ![Image 8](pictures/IMG_8.jpg) |
| Sweep   | Croker                |                                   | 8             | Blue                            | Women           |                   |                   |                   | Missing Extra BS                             | ![Image 9](pictures/IMG_9.jpg) |
| Sweep   | Concept 2             | Ultralight - Smoothie 2         | 10            | Black Suede for 8 (Dark Loom)    | Women           |                   |                   |                   |    On W3 rack                          | ![Image 10](pictures/IMG_0123.jpg) |
| Sweep   | Concept 2             | Ultralight - Smoothie 2         | 8            | Grey white tape | Women |                   |                   |                   |                                              | ![Image 11](pictures/IMG_0125.jpg) |
| Sweep   | Concept 2             | Old            | 10             | Swede, Yellow ends | Men |               |                   |                   |          Top of rack                                    | ![Image 14](pictures/IMG_0130.jpg) |
| Sweep   | Concept 2             | Old            | 8             | Black Suede                      | Women           |                   |                   |                   |         Also top of the rack                                     | ![Image 15](pictures/IMG_0129.jpg) |
| Spares  | Concept 2             | IDK         | 4 | Pink Collars              | Probably Men     |                   |                   |                   | Leant against boat house. Called Stewie                  | ![Image 16](pictures/IMG_0116.jpg) |
| Scull   | IDK             |                                   | 2             | Blue Handle                      | Men |                   |                   |                   | In corner                    | ![Image 17](pictures/IMG_0092.jpg) |
| Scull   | IDK            |                                   | 2             |                      |                 |                   |                   |                   | also behind boats against wall                   | ![Image 18](pictures/IMG_18.jpg) |

## Lifejackets

- Notes; *These are just the three that were in the boat house the ones Toby has and in Abingdon need to be added.*

| Type              | Make                  | Location | Year of Manufacture/Purchase | Colour    | Functional? | Serial No. | ISO Number | Notes                                                | Picture                                    |
|-------------------|-----------------------|----------|-----------------------------|-----------|-------------|------------|------------|------------------------------------------------------|-------------------------------------------|
| Buoyancy Aid       | IDK | BH     | Unknown                     | Red/Blue  | Sure |    |            |                                                      | ![Image 22](pictures/IMG_0204.jpg) |
| Buoyancy Aid       | Decathalon | BH     | Unknown                     | Red/Grey  | Sure |    |            |                                                      | ![Image 22](pictures/IMG_0203.jpg) |
| Manual Inflating  | Seago 150N Classic    | BH       | 03/2015                     | Red/Blue  | Yes (03/18) | 946229     | 12402-3/6   |                 They all need to be checked for inflation                       | ![Image 23](pictures/IMG_0202.jpg) |

## Coxboxes

- Notes; *So far these are just the ones from the BH the others need to be added. And all the status' need to be updated!*

| Make           | Description                                 | Serial No. of Cox Box | Serial No. of Battery | Date of Manufacture | Cost                             | Belongs to | Functional? | Comments | Possession                           | Picture                                    |
|----------------|---------------------------------------------|-----------------------|-----------------------|--------------------|----------------------------------|------------|-------------|------------|------------------------------------|-------------------------------------------|
| NK             | Blue and Red                               | 2047728               | 2071538               | 2014/15?            | £470 new (+£96 for microphone)  | Men        | Yes          | Says low battery when it's fine | BH |  ![Image 28](pictures/IMG_0207.jpg) |
| NK             | Blue and Red                               | 2047728               | 2071538               | 2014/15?            | £470 new (+£96 for microphone)  | Men        | Yes          | Says low battery when it's fine | BH |  ![Image 29](pictures/IMG_0196.jpg) |
| Active Tools   | Cox Orb Cobalt (pink, sky blue, zebra)     | CCA06Y39048           | unknown               | 2018/19?            | £465 (+ £15 p+p)                | Joint (mainly M1 use) | Yes |          |        BH                              | ![Image 31](pictures/IMG_0206.jpg) |

## Lights

- Notes; *This needs a lot of work. We should also just do a tidy and get rid of the lights we never use. The single also need to be fitted with velcro. And we're getting new lights soon that must be added here. Photos can be improved a lot. And stuff from Abingdon needs to be added.*

| Light                     | Description                            | Comment                                        | Picture                                     |
|---------------------------|----------------------------------------|------------------------------------------------|--------------------------------------------|
| 2 x Big Bulb Lights   |                                        | Probably not working              | ![Image 35](pictures/IMG_0208.jpg)           |
| 2x Brightside Bike lights       |       Amos' brilliant idea                                 | Should work! | ![Image 36](pictures/IMG_0211.jpg)        |
| 1x Fucked up contraption | Some kind of bulb light | No idea if it works | ![Image](pictures/IMG_0193.jpg) |

## Walkie-Talkies

- Notes; *I think we only have this one set*

| Make                          | Functional | Belongs to   | Base?                               | Comments | Picture                     |
|-------------------------------|------------|--------------|-------------------------------------|-------------------------------------|-----------|
| Motorola TLKR x2              | Yes        | Unknown      | Yes          | Comes with charging box and other stuff. | ![Image 43](pictures/IMG_0194.jpg)       |

## Headphones and Microphones

- Notes; *Better pictures needed. Ask coxes to do this.*

| Description                   | Brand       | Colour      | Functional? | Comment | Picture                                     |
|------------------------------|-------------|-------------|------------|--------------------------|------------------|
| 3x Head Band + Mic           | NK          | Blue/White  | Yes   |     | ![Image 44](pictures/IMG_0196.jpg)           |
| 2x Head Band + Mic           | Active Tools | Pink        | Yes    |   Lying on coxing cupboard floor, other in a box | ![Image 45](pictures/IMG_0186.jpg)           |

## Chargers, Cables and Accessories

- Notes; *This will be incredibly out of date. I have no idea about charges!, I've just deleted some things we clearly don't have anymore..., And there are lots of other things I haven't gotten down...*

| Type                         | Brand       | Functional? | Belongs to | Possession | Comments                           | Picture                                     |
|------------------------------|-------------|------------|------------|------------|------------------------------------|--------------------------------------------|
| Mini USB → USB Cable         | Active Tools | Yes        | Shared      | BH          | -                                  | ![Image 47](pictures/IMG_0187.jpg)           |
| Cox Orb Cable with Audio Jack | Active Tools | Yes        | Shared    | BH          | -                                  | ![Image 48](pictures/IMG_0186.jpg)           |
| Motorola Radio Case | Motorola | Yes        | Shared     | BH        | In coxing cupboard, seems to be one case inside another...                                | ![Image 48](pictures/IMG_0189.jpg)           |
| 2x Cox Orb Maintenance Pack      | -           | -          | Shared      | BH | IN coxing cupboard              | ![Image 55](pictures/IMG_0200.jpg) |
| Speed Coach XL                | NK          | Yes        | Men        | BH | With box         | ![Image 57](pictures/IMG_0191.jpg)           |
| 2x Seat Pad   | Oarsport    | Yes        | Unknown    | -          | In the coxing cupboard           | ![Image 62](pictures/IMG_0190.jpg)           |
| Seat Pad (Green/Red Label)    | Oarsport    | Yes        | Unknown    | -          | Lying in the bar    | ![Image 62](pictures/IMG_0234.jpg)           |
| Bottle of sun cream | Soltan Kids | Yes | Shared | BH | In changing rooms | ![Image 63](pictures/IMG_0162.jpg) |
| Expired plasters | | No | Shared | BH | In the first aid kit. | ![Image](pictures/IMG_0173.jpg) |
| First Aid kit | | Partly | Shared | BH | In changing rooms | ![Image](pictures/IMG_0168.jpg) |
| 4x Hudson rigger bags | Hudson | Yes | Shared | BH | In changing rooms | ![Image](pictures/IMG_0180.jpg) |
| 8x Bucket hats | | Yes | Shared | BH | In changing rooms | ![Image](pictures/IMG_0182.jpg) |
| 3x Hudson Water Bottles | Hudson | Yes | Shared | BH | In changing rooms | ![Image](pictures/IMG_0184.jpg) |
| Stopwatch | | Yes | Shared | BH | In coxing cupboard | ![Image](pictures/IMG_0185.jpg) |
| 5x Automatic plug timers | | Maybe... | Shared | BH | In coxing cupboard | ![Image](pictures/IMG_0197.jpg) |
| Dry bag yellow | | Should | Shared | BH | In coxing cupboard | ![Image](pictures/IMG_0201.jpg) | 
| Random batteries | | Who knows... | Shared | BH | | ![Image](pictures/IMG_0209.jpg) | 
| Waterproof phone case | | Probably not | Shared | BH | | ![Image](pictures/IMG_0209.jpg) | 
| Tape measure | | Yes | shared | BH | | ![Image](pictures/IMG_0212.jpg) |
| Various Boat Ties | | Yes | Shared | BH | | ![Image](pictures/IMG_0212.jpg) |

## Loanable kit

- Notes; *I have no idea where any of this is. Should be worth chasing up older members who hopefully haven't pocketed it!*

## Ergs

- Notes; *Needs to be completely redone. And should be recorded when they were last serviced...*

| Type     | PM Model | PM Serial Code | PM Date    | Lifetime Metres (as of 09/2018) | Functional? | Location    | Other Comments    | Picture                                     |
|----------|----------|----------------|------------|----------------------------------|------------|-------------|-------------------|--------------------------------------------|
| Dynamic  | 4        | 410037781      | 30/06/2010 | 1,301,051                        | Yes        | College GYM          |                   | ![Image 63](pictures/IMG_63.jpg)           |
| Model C  | 3        | 400078422      | 21/9/2010  | 4,815,356                        | Yes        | BH          | Wooden handle     | ![Image 64](pictures/IMG_0214.jpg)           |
| Model C  | 3        | 300114759      | ???        | ???                              | Yes, no display (probably batteries) | BH | Base screws need tightening | ![Image 65](pictures/IMG_0214.jpg) |
| Model C  | 3        | 400078389      | ???        | ???                              | Yes, no display (probably batteries) | BH |                   | ![Image 66](pictures/IMG_0214.jpg)           |
| Model C  | 3        |                |            |                                  | No (Dead Batteries) | College Gym | | - |
| Model C  | 3        |                |            |                                  | No (Dead Batteries) | College Gym | | - |
| Model C  | 3        |                |            |                                  | No (Dead Batteries) | College Gym | | - |
| Model C  | 3        |                |            |                                  | No (Dead Batteries) | College Gym | | - |
| Model D  | 5        |                |            |                                  | Yes        | College Gym | | - |
| Model D  | 5        |                |            |                                  | Yes        | College Gym | | - |
| Model D  | 5        |                |            |                                  | Yes        | College Gym | | - |
| Model D  | 5        |                |            |                                  | Yes        | College Gym | | - |
| Model D  | 5        |                |            |                                  | Yes        | College Gym | | - |

## Other

- Notes; *This like tressels, cleaning stuff, barbeques, and food stuffs. We should probably also get an inventory of everything Stewie holds in the boatclub's name*

| Item | Condition | Location | Picture | 
|----|------|-----|-----|
| 4x brand new tressels | Perfect | BH | ![Image](pictures/IMG_0090.jpg) |
| Random box with garden hose | Fine | BH | ![Image](pictures/IMG_0092.jpg) |
| Kayaking paddle | Good | BH | ![Image](pictures/IMG_0093.jpg) |
| 2x liferings | Should be checked | BH | ![Image](pictures/IMG_0095.jpg) |
| 1x broken tressel | Now replaced | BH | ![Image](pictures/IMG_0097.jpg) |
| Various riggers | No idea | BH | ![Image](pictures/IMG_0098.jpg) |
| 2x cleaning buckets, 3x sponges | Fairly new | BH | ![Image](pictures/IMG_0100.jpg) |
| Rubbish bin | Good | BH | ![Image](pictures/IMG_0102.jpg) |
| Empacher rests | Good | BH | ![Image](pictures/IMG_0102.jpg |
| Pressure washer attachment | Good | BH | ![Image](pictures/IMG_0104.jpg) |
| Megaphone | no idea | BH | ![image](pictures/IMG_0105.jpg) |
| Cleaning hose | no idea | BH | ![image](pictures/IMG_0105.jpg) |
| Lifering | should be checked | BH | ![image](pictures/IMG_0105.jpg) |
| More riggers and 3 poles | fine | BH | ![image](pictures/IMG_0106.jpg) |
| Throw rope | Should be checked | BH | ![image](pictures/IMG_0107.jpg) |
| Even more random riggers | no idea | BH | ![image](pictures/IMG_0116.jpg) | 
| Random hatch covers and fire extinguisher and step ladder | no idea | BH | ![image](pictures/IMG_0118.jpg) | 
| First aid kit | desperately needs to be checked | On the back wall of the boat house. Under the stairs. | ![image](pictures/IMG_0131.jpg) |
| Various collection of random cleaning products. 18x big loo rolls. 48x paper towel packs. 25liters soap. 6 bottles toilet cleaner. 10x Cleaning clothes. 24 more rolls of paper.   | New |  Back of the boat house. | ![image](pictures/IMG_0133.jpg) |
| Titan pressure washer | We should really figure out a test to check that it does indeed work. | Back of the boat house. | ![image](pictures/IMG_0142.jpg) |
| Even more riggers | Now idea | Back of the boat house | ![image](pictures/IMG_0143.jpg) | 
| Cleaning bucket and random collection of rubbish include lots of empacher seats | Various, mostly don't know | BH | ![image](pictures/IMG_0152.jpg) | 
| Barbeque 1 | Probably works | BH | ![image](pictures/IMG_0169.jpg) |
| Smaller Barbeque | Probably works | BH | ![image](pictures/IMG_0170.jpg) |
| Lots of chairs and some tables and a bin | Probably work | BH upstairs | ![image](pictures/IMG_0218.jpg) |
| 2 cleaning buckets | Probably work | BH Upstairs | ![image](pictures/IMG_0221.jpg) |
| Vacuum cleaner | no idea | BH Upstairs | ![image](pictures/IMG_0224.jpg) |
| Fire extinguisher | checked | BH upstairs | ![image](pictures/IMG_0225.jpg) | 
| 2 brooms and a mop | should work | BH upstairs | ![image](pictures/IMG_0226.jpg) |
| BBQ lighter fuel | should work | Boat house kitchen | ![image](pictures/IMG_0233.jpg) | 
| Individually wrapped firelighters | Tiger Tim | Should | Shared | BH | | ![Image](pictures/IMG_0205.jpg) | 
| Kettle | good | BH | ![Image](pictures/IMG_0234.jpg) | 
| Sandwhich maker | good | BH | ![Image](pictures/IMG_0242.jpg) |
| 2 Mugs, BBQ utensils | Dirty and unwashed | in BH sink | ![image](pictures/IMG_0244.jpg) |
| Food stuffs | Should be updated regularly | in BH cupboard | ![Image](pictures/IMG_0246.jpg) |
| 3x new mop heads, bin bags | Good | in BH Kitchen cupboard | ![image](pictures/IMG_0253.jpg) | 
| Plastic ponchos various | Good | IN BH kitchen cupboard | ![image](pictures/IMG_0255.jpg) |
| Barbeque 3 | Good | On BH terrace | ![image](pictures/IMG_0268.jpg) |
























